/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package dataStructures;

import java.io.Serializable;

public class Enlace<E> implements Serializable{

	// CONSTANTES
	
	private static final long serialVersionUID = -8574705367001011452L;
	
	// VARIABLES
	
	/**
	 * Nodo al que se est� enlazado
	 */
	private E destino;
	/**
	 * Peso del enlace
	 */
	private Integer peso;

	// METODOS
	
	/**
	 * Constructor de la clase
	 * <n>post: </n> se crea un enlace con peso
	 */
	public Enlace(E destino, Integer peso){
		this.destino = destino;
		this.peso = peso;
	}
	/**
	 * Constructor de la clase
	 * <n>post: </n> se crea un enlace sin peso
	 */
	public Enlace(E destino){
		this.destino = destino;
		this.peso = -1;
	}
	/**
	 * Se modifica el peso del enlace
	 * @param peso, nuevo peso del enlace
	 */
	public void modificar(Integer peso){
		this.peso = peso;
	}
	/**
	 * Nodo al que apunta el enlace
	 * @return destino del enlace
	 */
	public E getDestino(){
		return destino;
	}
	/**
	 * Retorna el peso del enlace
	 * @return peso
	 */
	public Integer getPeso(){
		return peso;
	}
}