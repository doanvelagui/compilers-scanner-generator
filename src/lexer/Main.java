package lexer;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Scanner;
import scannerGenerator.Lexer;
import javax.swing.JFileChooser;

public class Main {
	public static void main(String[] args){
		Lexer lexer = null;
		try{
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("data/prueba.dv"));
			lexer = (Lexer)in.readObject();
			in.close();}
		catch(Exception e){
			System.err.println("ERROR: al cargar archivo prueba.dv.");}
		if(lexer!=null){
			System.out.println("=== Ejemplo ===\n\n � Ingresar direccion del archivo a analizar.\n � Ingresar 'exit' para salir.\n � Ingresar 'select' para seleccionar archivo.");
			Scanner scan = new Scanner(System.in);
			String read = "hola";
			while(!read.equals("exit")){
				System.out.print("\nIngreso: ");
				read = scan.next();
				if(read.equals("exit")){
					System.out.println("Saliendo...");
					break;}
				if(read.equals("select")){
					JFileChooser chooser = new JFileChooser("./");
					int option = chooser.showOpenDialog(null);
					if( option == JFileChooser.APPROVE_OPTION ) lexer.tokenizer(chooser.getSelectedFile( ).getPath());
					else lexer.tokenizer(read);}}}}}