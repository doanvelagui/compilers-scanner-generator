/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package automataGenerator;

import java.util.*;


/**
 * Analizador de expresion regular r y cadena w
 * @author doanvelagui
 */
public class Evaluador {
	
	/**
	 * Define el alfabeto basado en la expresion regular 
	 * @param pila, donde se encuentra la epresion regular
	 * @param alfabeto, donde se almacena el alfabeto definido
	 */
	public void definirAlfabeto(ArrayList<Integer> pila, ArrayList<Integer> alfabeto){
		for (int n=0;n<pila.size();n++)
			if(pila.get(n)!=ExpresionRegular.par1 && pila.get(n)!=ExpresionRegular.par2 && 
					pila.get(n)!=ExpresionRegular.eps && pila.get(n)!=ExpresionRegular.pip && 
					pila.get(n)!=ExpresionRegular.ast && !alfabeto.contains(pila.get(n)))
				alfabeto.add(pila.get(n));
	}
	/**
	 * Construye un arbol basado en la expresion regular
	 * @param pila, caracteres leidos
	 * @param er, arbol de expresion regular
	 * @return root, arbol construido en base a la expresion regular 
	 */
	public ExpresionRegular expresionRegular(ArrayList<Integer> pila, ArrayList<Integer> alfabeto){
		if(pila.size()>1){
			Integer caracter = pila.remove(pila.size()-1);
			Integer nextChar = pila.get(pila.size()-1);
			ExpresionRegular root = new ExpresionRegular();
			
			
			//	si caracter pertence a alfabeto o es epsilon
			if(alfabeto.contains(caracter)||caracter==ExpresionRegular.eps){
				if(alfabeto.contains(nextChar) || nextChar==ExpresionRegular.eps || nextChar==ExpresionRegular.par2 
						|| nextChar==ExpresionRegular.ast){
					root.cambiarSimbolo(ExpresionRegular.cat);
					root.cambiarDerecha(new ExpresionRegular(caracter));
					root.cambiarIzquierda(expresionRegular(pila, alfabeto));
				}
				else if(nextChar==ExpresionRegular.pip){
					Integer neNextChar = pila.get(pila.size()-2);
					if(alfabeto.contains(neNextChar) || neNextChar==ExpresionRegular.eps || neNextChar==ExpresionRegular.ast 
							|| neNextChar==ExpresionRegular.par2){
						root.cambiarSimbolo(ExpresionRegular.pip);
						ExpresionRegular derecha = new ExpresionRegular(pila.remove(pila.size()-1));
						derecha.cambiarDerecha(new ExpresionRegular(caracter));
						root.cambiarDerecha(new ExpresionRegular(caracter));
						root.cambiarIzquierda(expresionRegular(pila, alfabeto));
					}
				}
			}
			
			
			//	si caracter es asterisco (Kleene)
			else if(caracter==ExpresionRegular.ast){
				if(alfabeto.contains(nextChar)){
					if(pila.size()==1){
						root.cambiarSimbolo(ExpresionRegular.ast);
						root.cambiarIzquierda(new ExpresionRegular(pila.remove(pila.size()-1)));
					}
					else{
						int neNextChar = pila.get(pila.size()-2);
						if(alfabeto.contains(neNextChar) || nextChar==ExpresionRegular.eps || 
								neNextChar==ExpresionRegular.par2 || neNextChar==ExpresionRegular.ast){
							root.cambiarSimbolo(ExpresionRegular.cat);
							ExpresionRegular derecha=new ExpresionRegular(caracter);
							derecha.cambiarIzquierda(new ExpresionRegular(pila.remove(pila.size()-1)));
							root.cambiarDerecha(derecha);
							root.cambiarIzquierda(expresionRegular(pila, alfabeto));
						}
						else if(neNextChar==ExpresionRegular.pip){
							ExpresionRegular derecha=new ExpresionRegular(caracter);
							derecha.cambiarIzquierda(new ExpresionRegular(pila.remove(pila.size()-1)));
							root.cambiarSimbolo(pila.remove(pila.size()-1));
							root.cambiarDerecha(derecha);
							root.cambiarIzquierda(expresionRegular(pila, alfabeto));
						}
					}
				}
				else if(nextChar==ExpresionRegular.ast){
					root = expresionRegular(pila,alfabeto);
				}
				else if(nextChar==ExpresionRegular.par2){
					pila.remove(pila.size()-1);
					ArrayList<Integer> subpila = subpila(pila);
					if(pila.isEmpty()){
						root.cambiarSimbolo(ExpresionRegular.ast);
						root.cambiarIzquierda(expresionRegular(subpila, alfabeto));
					}
					else{
						nextChar = pila.get(pila.size()-1);
						if(alfabeto.contains(nextChar) || nextChar==ExpresionRegular.eps || nextChar==ExpresionRegular.ast 
								|| nextChar==ExpresionRegular.par2){
							root.cambiarSimbolo(ExpresionRegular.cat);
							ExpresionRegular derecha=new ExpresionRegular(caracter);
							derecha.cambiarIzquierda(expresionRegular(subpila, alfabeto));
							root.cambiarDerecha(derecha);
							root.cambiarIzquierda(expresionRegular(pila, alfabeto));
						}
						else if(nextChar==ExpresionRegular.pip){
							root.cambiarSimbolo(pila.remove(pila.size()-1));
							ExpresionRegular derecha = new ExpresionRegular(caracter);
							derecha.cambiarIzquierda(expresionRegular(subpila, alfabeto));
							root.cambiarDerecha(derecha);
							root.cambiarIzquierda(expresionRegular(pila, alfabeto));
						}
					}
				}
			}
			//Si caracter es parentesis cerrado ')'
			else if(caracter==ExpresionRegular.par2){
				ArrayList<Integer> subpila = subpila(pila);
				if(pila.isEmpty()){
					root = expresionRegular(subpila, alfabeto);
				}
				else{
					nextChar=pila.get(pila.size()-1);
					if(alfabeto.contains(nextChar) || nextChar==ExpresionRegular.eps || nextChar==ExpresionRegular.ast
							|| nextChar == ExpresionRegular.par2){
						root.cambiarSimbolo(ExpresionRegular.cat);
						root.cambiarDerecha(expresionRegular(subpila, alfabeto));
						root.cambiarIzquierda(expresionRegular(pila, alfabeto));
					}
					else if(nextChar==ExpresionRegular.pip){
						root.cambiarSimbolo(pila.remove(pila.size()-1));
						root.cambiarDerecha(expresionRegular(subpila, alfabeto));
						root.cambiarIzquierda(expresionRegular(pila, alfabeto));
					}
				}
			}
			return root;
		}
		else if(pila.size()==1)
			return new ExpresionRegular(pila.remove(pila.size()-1));
		else
			return null;
	}
	
	/**
	 * Crea una subpila basado en los parentesis
	 */
	private ArrayList<Integer> subpila(ArrayList<Integer> pila){
		int contador = 1;
		int index = -1;
		for (int m=pila.size()-1; contador!=0;m--){
			if(pila.get(m)==ExpresionRegular.par2)
				contador++;
			else if(pila.get(m)==ExpresionRegular.par1){
				contador--;
				index=m;
			}
		}
		
		ArrayList<Integer> subpila = new ArrayList<Integer>();
		pila.remove(index);
		while(pila.size()>index)
			subpila.add(pila.remove(index));
		return subpila;
	}
}