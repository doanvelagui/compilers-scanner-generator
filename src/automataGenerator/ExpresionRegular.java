/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package automataGenerator;

public class ExpresionRegular {
	
	//	CONSTANTES
	
	public final static Integer cat = 65536;
	public final static Integer pip = 65537;
	public final static Integer ast = 65538;
	public final static Integer eps = 65539;
	public final static Integer par1 = 65540;
	public final static Integer par2 = 65541;
	public final static Integer shp = 65542;
	
	//	VARIABLES
	
	private ExpresionRegular izquierda;
	private ExpresionRegular derecha;
	private Integer simbolo;
	
	//	METODOS
	
	/**
	 * Constructor
	 */
	public ExpresionRegular(){
		izquierda = null;
		derecha = null;
	}
	/**
	 * Constructor
	 * @param simbolo asociado al nodo
	 */
	public ExpresionRegular(Integer simbolo){
		this();
		this.simbolo = simbolo;
	}
	/**
	 * Cambia el valor del nodo izquierdo
	 * @param izquierda, nuevo nodo izquierdo
	 */
	public void cambiarIzquierda(ExpresionRegular izquierda){
		this.izquierda = izquierda;
	}
	/**
	 * Cambia el valor del nodo derecho
	 * @param derecha, nuevo nodo derecho
	 */
	public void cambiarDerecha(ExpresionRegular derecha){
		this.derecha = derecha;
	}
	/**
	 * Cambia el simbolo asociado al nodo.
	 * @param simbolo, nuevo valor para el nodo
	 */
	public void cambiarSimbolo(Integer simbolo){
		this.simbolo = simbolo;
	}
	/**
	 * Retorna el nodo izquierdo
	 * @return nodo izquierdo
	 */
	public ExpresionRegular darIzquierda(){
		return izquierda;
	}
	/**
	 * Retorna el nodo derecho
	 * @return nodo derecho
	 */
	public ExpresionRegular darDerecha(){
		return derecha;
	}
	/**
	 * Retorna el simbolo asociado al nodo
	 * @return simbolo
	 */
	public Integer darSimbolo(){
		return simbolo;
	}
}