/*
 * Universidad del Valle de Guatemala
 * Dise�o de Lenguajes de Programaci�n
 * 
 * Donald Antonio Vel�squez Aguilar
 * Marzo del 2010
 */

package automataGenerator;

import dataStructures.*;
import java.util.*;

public class AFD_Directo {
	
	// VARIABLES GLOBALES
	protected ArrayList<ExpresionRegular> nodos;
	protected ArrayList<Integer> nullable;
	protected ArrayList<ArrayList<Integer>> firstpos;
	protected ArrayList<ArrayList<Integer>> lastpos;
	protected ArrayList<ArrayList<Integer>> followpos;
	protected ArrayList<Integer> indices;
	
	// VARIABLE LOCAL
	protected Grafo automata;
	
	/**
	 * Constructor de la clase
	 * @param afn, automata finito no determinista
	 * @param alfabeto, alfabeto asociado al afn
	 */
	public AFD_Directo(ExpresionRegular expresionRegularExtendida, ArrayList<Integer> alfabeto){
		automata = (expresionRegularExtendida==null) ? null : Directo(expresionRegularExtendida, alfabeto);
	}
	
	private Grafo Directo(ExpresionRegular r, ArrayList<Integer>alf){
		nodos = darNodos(r);
		darNullable(alf);
		darFirstLastPos(alf);
		darFollowPos(alf);
		Grafo afn = new Grafo();
		afn.estadoInicial=0;
		
		ArrayList<ArrayList<Integer>> nuevosEstados = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> estadosNoMarcados = new ArrayList<ArrayList<Integer>>();
		
		ArrayList<Integer> estadoInicial = firstpos.get(nodos.indexOf(r));
		ArrayList<Integer> estado = new ArrayList<Integer>();
		
		nuevosEstados.add(estadoInicial);
		estadosNoMarcados.add(estadoInicial);
		
		while(!estadosNoMarcados.isEmpty()){
			estado = estadosNoMarcados.remove(estadosNoMarcados.size()-1);
			int estadoActual = contiene(nuevosEstados, estado);
			
			for(int n=0;n<alf.size();n++){
				int simbolo = alf.get(n);
				ArrayList<Integer> nuevoEstado = new ArrayList<Integer>();
				for(int m=0;m<estado.size();m++){
					if(simbolo==nodos.get(estado.get(m)).darSimbolo()){
						int index = indices.indexOf(estado.get(m)); 
						for(int o=0;o<followpos.get(index).size();o++){
							if(!nuevoEstado.contains(followpos.get(index).get(o)))
								nuevoEstado.add(followpos.get(index).get(o));
						}
					}
				}
				
				int index = contiene(nuevosEstados,nuevoEstado);
				if(index==-1){
					nuevosEstados.add(nuevoEstado);
					estadosNoMarcados.add(nuevoEstado);
					index = contiene(nuevosEstados, nuevoEstado);
				}
				if(index>-1)
					afn.addEdge(estadoActual, index, alf.get(n));
			}
		}
		
		for(int n=0; n<nuevosEstados.size(); n++){
			estado = nuevosEstados.get(n);
			if(estado.contains(nodos.indexOf(r.darDerecha())))
				if(!afn.estadosFinales.contains(n))
					afn.estadosFinales.add(n);
		}
		
		return afn;
	}
	
	/**
	 * Indica el indice del conjunto si esta contenido en conjuntos
	 * @param conjuntos, conjunto de conjuntos
	 * @param conjunto, conjunto
	 * @return indice >= 0, -1 si no lo contiene, -2 si conjunto esta vacio
	 */
	private int contiene(ArrayList<ArrayList<Integer>> conjuntos, ArrayList<Integer> conjunto){
		if(!conjunto.isEmpty() && !conjuntos.isEmpty()){
			for(int n=0;n<conjuntos.size();n++){
				ArrayList<Integer> set = conjuntos.get(n);
				if(set.size()==conjunto.size()){
					int count = 0, index=n;
					for(int m=0;m<conjunto.size();m++)
						if(set.contains(conjunto.get(m)))count++;
					if(count==conjunto.size()) return index;
				}
			}return -1;
		}return -2;
	}
	
	/**
	 * Almacena los nodos en una lista y la retorna
	 * @param expresion regular
	 * @return nodes, lista.
	 */
	private ArrayList<ExpresionRegular> darNodos(ExpresionRegular r){
		ArrayList<ExpresionRegular> nodes = new ArrayList<ExpresionRegular>();
		if(r.darIzquierda()!=null)
			nodes.addAll(darNodos(r.darIzquierda()));
		if(r.darDerecha()!=null)
			nodes.addAll(darNodos(r.darDerecha()));
		nodes.add(r);
		return nodes;
	}
	
	/**
	 * Define la funcion nullable de los nodos del arbol
	 * @param alfabeto
	 */
	private void darNullable(ArrayList<Integer>alf){
		nullable = new ArrayList<Integer>();
		for(int n=0;n<nodos.size();n++){
			int simbolo = nodos.get(n).darSimbolo();
			if(alf.contains(simbolo)){
				nullable.add(0);
			}else if(simbolo==ExpresionRegular.eps){
				nullable.add(1);
			}else if(simbolo==ExpresionRegular.shp){
				nullable.add(0);
			}else if(simbolo==ExpresionRegular.pip){
				int index1 = nodos.indexOf(nodos.get(n).darIzquierda());
				int index2 = nodos.indexOf(nodos.get(n).darDerecha());
				if(nullable.get(index1)==1 || nullable.get(index2)==1)
					nullable.add(1);
				else
					nullable.add(0);
			}else if(simbolo==ExpresionRegular.cat){
				int index1 = nodos.indexOf(nodos.get(n).darIzquierda());
				int index2 = nodos.indexOf(nodos.get(n).darDerecha());
				if(nullable.get(index1)==1 && nullable.get(index2)==1)
					nullable.add(1);
				else
					nullable.add(0);
			}else if(simbolo==ExpresionRegular.ast){
				nullable.add(1);
			}
		}
	}
	
	/**
	 * Define las funciones lastpos y firstpos
	 * @param alfabeto
	 */
	private void darFirstLastPos(ArrayList<Integer>alf){
		firstpos = new ArrayList<ArrayList<Integer>>();
		lastpos = new ArrayList<ArrayList<Integer>>();
		for(int n=0;n<nodos.size();n++){
			int simbolo = nodos.get(n).darSimbolo();
			ArrayList<Integer> first = new ArrayList<Integer>();
			ArrayList<Integer> last = new ArrayList<Integer>();
			if(alf.contains(simbolo) || simbolo==ExpresionRegular.shp){
				first.add(n);
				last.add(n);
				firstpos.add(first);
				lastpos.add(last);
			}else if(simbolo==ExpresionRegular.eps){
				firstpos.add(first);
				lastpos.add(last);
			}else if(simbolo==ExpresionRegular.pip){
				int index1 = nodos.indexOf(nodos.get(n).darIzquierda());
				int index2 = nodos.indexOf(nodos.get(n).darDerecha());
				
				first.addAll(firstpos.get(index1));
				first.addAll(firstpos.get(index2));
				firstpos.add(first);
				
				last.addAll(lastpos.get(index1));
				last.addAll(lastpos.get(index2));
				lastpos.add(last);
			}else if(simbolo==ExpresionRegular.cat){
				int index1 = nodos.indexOf(nodos.get(n).darIzquierda());
				int index2 = nodos.indexOf(nodos.get(n).darDerecha());
				
				first.addAll(firstpos.get(index1));
				if(nullable.get(index1)==1)
					first.addAll(firstpos.get(index2));
				firstpos.add(first);
				
				last.addAll(lastpos.get(index2));
				if(nullable.get(index2)==1)
					last.addAll(lastpos.get(index1));
				lastpos.add(last);
			}else if(simbolo==ExpresionRegular.ast){
				int index1 = nodos.indexOf(nodos.get(n).darIzquierda());
				firstpos.add(firstpos.get(index1));
				lastpos.add(lastpos.get(index1));
			}
		}
	}
	
	/**
	 * Define las funciones lastpos y firstpos
	 * @param alfabeto
	 */
	private void darFollowPos(ArrayList<Integer>alf){
		followpos = new ArrayList<ArrayList<Integer>>();
		indices = new ArrayList<Integer>();
		for(int n=0;n<nodos.size();n++){
			int simbolo = nodos.get(n).darSimbolo();
			ArrayList<Integer> follow = new ArrayList<Integer>();
			if(alf.contains(simbolo)){
				indices.add(n);
				for(int m=0;m<nodos.size();m++){
					int simbolo2 = nodos.get(m).darSimbolo();
					if(simbolo2==ExpresionRegular.cat){
						int index1 = nodos.indexOf(nodos.get(m).darIzquierda());
						int index2 = nodos.indexOf(nodos.get(m).darDerecha());
						
						if(lastpos.get(index1).contains(n))
							follow.addAll(firstpos.get(index2));
					}else if(simbolo2==ExpresionRegular.ast){
						if(lastpos.get(m).contains(n))
							follow.addAll(firstpos.get(m));
					}
				}followpos.add(follow);
			}
		}
	}
	/**
	 * Retorna el grafo asociado al automata
	 * @return
	 */
	public Grafo darAutomata(){
		return automata;
	}
}
