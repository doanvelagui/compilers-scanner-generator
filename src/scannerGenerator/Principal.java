package scannerGenerator;

import java.io.*;
import java.util.Scanner;
import java.util.*;
import javax.swing.JFileChooser;

import automataGenerator.*;
import dataStructures.*;

public class Principal {
	
	//	VARIABLES 
	/** Nombre para generar proyecto */
	protected String fileName;
	
	/** Nombre para el proyecto */
	protected String projectName;
	
	/** Tokens y Estados Finales */
	protected HashMap<Integer, String> tokens;
	
	/** Caracters ignorados */
	protected ArrayList<Integer> ignorados;
	
	/** Grafo */
	protected Grafo grafo;
	
	//	METODOS
	
	/**
	 * construye las estructuras necesarias
	 */
	public void build(scannerGenerator.Scanner myScanner) throws Exception{
		fileName = myScanner.fileName.toLowerCase();
		projectName = myScanner.projectName;
		ignorados = myScanner.ignorados;
		tokens = new HashMap<Integer, String>();
		
		grafo = new Grafo();
		grafo.estadoInicial = 0;
		grafo.estadosFinales = new ArrayList<Integer>();
		
		int lastEstado = 1;
		
		ArrayList<String> keys = myScanner.getKeys();
		for(int n=0;n<keys.size();n++){
			ArrayList<Integer> pila = myScanner.expReg.get(keys.get(n));
			ArrayList<Integer> alfabeto = new ArrayList<Integer>();
			Evaluador evaluador = new Evaluador();
			
			evaluador.definirAlfabeto(pila, alfabeto);
			
			ExpresionRegular er = new ExpresionRegular();
			er.cambiarSimbolo(ExpresionRegular.cat);
			er.cambiarDerecha(new ExpresionRegular(ExpresionRegular.shp));
			er.cambiarIzquierda(evaluador.expresionRegular(pila,alfabeto));
			AFD_Directo grafito = new AFD_Directo(er, alfabeto);
			
			ArrayList<Integer> estadosFinales = grafito.darAutomata().estadosFinales;
			for(int m=0;m<estadosFinales.size();m++){
				tokens.put(estadosFinales.get(m)+lastEstado, keys.get(n));
				grafo.estadosFinales.add(estadosFinales.get(m)+lastEstado);
			}grafo.addEdge(grafo.estadoInicial, grafito.darAutomata().estadoInicial+lastEstado, ExpresionRegular.eps);
			
			Integer[] nodos = grafito.darAutomata().getNodos();
			for(int m=0;m<nodos.length;m++){
				LinkedList<Enlace<Integer>> edges = grafito.darAutomata().getEdges(nodos[m]);
				while(edges!=null && !edges.isEmpty()){
					Enlace<Integer> e = edges.remove();
					grafo.addEdge(nodos[m]+lastEstado, e.getDestino()+lastEstado, e.getPeso());
				}
			}lastEstado = grafo.sizeNodes();
		}
	}
	
	/**
	 * Genera el proyecto.
	 */
	public void generar(){
		Lexer lexer = new Lexer(grafo, tokens, ignorados);
		escribirAF(grafo, "data/"+fileName+".dot");
		try{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data/"+fileName+".dv"));
			out.writeObject(lexer);
			out.close();
		}catch(Exception e){
			System.err.println("ERROR: al generar archivo "+fileName+".dv");
		}escribirMain();
	}
	
	/**
	 * MAIN
	 * @param args	arguments
	 */
	public static void main(String args[]){
		Principal p = new Principal();
		System.out.println("== GENERADOR DE ANALIZADORES LEXICOS� ==\n\n � Ingresar direcci�n de archivo ATG\n � " +
				"Ingresar 'select' para seleccionar archivo.\n � Ingresar 'exit' para salir.");
		Scanner scan = new Scanner(System.in);
		String read = "hola";
		while(!read.equals("exit")){
			System.out.print("\nIngreso: ");
			read = scan.next();
			if(read.equals("exit")){
				System.out.println("Saliendo...");
				break;
			}
			if(read.equals("select")){
				JFileChooser chooser = new JFileChooser("./data");
				int option = chooser.showOpenDialog(null);
	            if( option == JFileChooser.APPROVE_OPTION ){
	            	try{
						scannerGenerator.Scanner ms = new scannerGenerator.Scanner();
						ms.readAndBuild(chooser.getSelectedFile().getPath());
						System.out.println("\nLectura de archivo "+chooser.getSelectedFile().getName()+" exitosa.");
						p.build(ms);
						System.out.println("Generacion de automata exitosa.");
						p.generar();
						System.out.println("Generacion de proyecto exitosa.");
					}catch(Exception e){
						System.err.println("ERROR: "+e.getMessage());
					}
	            }
			}else{
				if(!read.toUpperCase().endsWith(".ATG"))
					read+=".ATG";
				try{
					scannerGenerator.Scanner ms = new scannerGenerator.Scanner();
					ms.readAndBuild("data/"+read);
					System.out.println("\nLectura de archivo "+read+" exitosa.");
					p.build(ms);
					System.out.println("Generacion de automata exitosa.");
					p.generar();
					System.out.println("Generacion de proyecto exitosa.");
				}catch(Exception e){
					System.err.println("ERROR: "+e.getMessage());
				}
			}
		}
	}
	
	
	
	/**
	 * Empleado para generar archivo dot
	 * @param automata	grafo a escribir en archivo dot
	 * @param path	direccion donde guardar
	 */
	private void escribirAF(Grafo automata, String path){
		try{
			PrintWriter out = new PrintWriter(new File(path));
			Integer[] estados = automata.getNodos();
			out.println("digraph finite_state_machine {");
			out.println("\trankdir=LR;");
			out.println("\tsize=\"12\"");
			out.println("\tnode [shape=triplecircle]; "+automata.estadoInicial+";");
			out.print("\tnode [shape=doublecircle]; ");
			for(int n=0;n<automata.estadosFinales.size();n++)
				out.print(automata.estadosFinales.get(n)+" ");
			out.println(";");
			out.println("\tnode [shape=circle];");
			for( int n=0; n<estados.length;n++){
				LinkedList<Enlace<Integer>> edges = automata.getEdges(estados[n]);
				if(edges!=null){
					for(int m=0;m<edges.size();m++){
						Enlace<Integer> e = edges.get(m);
						String label = (e.getPeso()==ExpresionRegular.eps) ? " " : ""+e.getPeso();
						out.println("\t"+estados[n]+" -> "+e.getDestino()+" [label=\""+label+"\"];");
					}
				}
			}
			out.println("}");
			out.close();
		}catch(IOException ioe){
			System.err.println("ERROR: al generar archivo "+fileName+".dot");
		}
	}
	
	private void escribirMain(){
		String write = "package lexer;\nimport java.io.FileInputStream;\nimport java.io.ObjectInputStream;\n"+
			"import java.util.Scanner;\nimport scannerGenerator.Lexer;\nimport javax.swing.JFileChooser;\n\n"+
			"public class Main {\n\tpublic static void main(String[] args){\n\t\tLexer lexer = null;\n\t\t"+ 
			"try{\n\t\t\tObjectInputStream in = new ObjectInputStream(new FileInputStream(\"data/"+fileName+
			".dv\"));\n\t\t\tlexer = (Lexer)in.readObject();\n\t\t\tin.close();}\n\t\tcatch(Exception e){\n\t\t\t"+
			"System.err.println(\"ERROR: al cargar archivo "+fileName+".dv.\");}\n\t\tif(lexer!=null){\n\t\t\t"+
			"System.out.println(\"=== "+projectName+" ===\\n\\n � Ingresar direccion del archivo a analizar.\\n � " +
			"Ingresar 'exit' para salir.\\n � Ingresar 'select' para seleccionar archivo.\");\n\t\t\tScanner "+
			"scan = new Scanner(System.in);\n\t\t\tString read = \"hola\";\n\t\t\twhile(!read.equals(\"exit\")){"+
			"\n\t\t\t\tSystem.out.print(\"\\nIngreso: \");\n\t\t\t\tread = scan.next();\n\t\t\t\tif(read.equals(" +
			"\"exit\")){\n\t\t\t\t\tSystem.out.println(\"Saliendo...\");\n\t\t\t\t\tbreak;}\n\t\t\t\tif(read.equ"+
			"als(\"select\")){\n\t\t\t\t\tJFileChooser chooser = new JFileChooser(\"./\");\n\t\t\t\t\tint option "+
			"= chooser.showOpenDialog(null);\n\t\t\t\t\tif( option == JFileChooser.APPROVE_OPTION ) lexer.tokeniz" +
			"er(chooser.getSelectedFile( ).getPath());\n\t\t\t\t\telse lexer.tokenizer(read);}}}}}";
		
		try{
			PrintWriter out = new PrintWriter(new File("src/lexer/Main.java"));
			out.print(write);
			out.close();
		}catch(Exception e){
			System.err.println("ERROR: al generar archivo Main.java");
		}
	}
}