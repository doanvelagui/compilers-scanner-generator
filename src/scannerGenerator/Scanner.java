package scannerGenerator;

import java.io.*;
import java.util.*;

import automataGenerator.ExpresionRegular;

public class Scanner {
	
	//	CONSTANTES
	
	/** Valor maximo para caracteres */
	public static final Integer MAX_CHAR = 256;
	
	//	VARIABLES
	
	/** Nombre para generar proyecto */
	protected String fileName;
	
	/** Nombre para el proyecto */
	protected String projectName;
	
	/** Conjuntos de caracteres */
	protected HashMap<String, ArrayList<Integer>> charSets;
	
	/** Expresiones Regulares */
	protected HashMap<String, ArrayList<Integer>> expReg;
	
	/** Conjunto a ignorar */
	protected ArrayList<Integer> ignorados;
	
	/** Nombres de tokens */
	private ArrayList<String> keys;
	
	//	 METODOS

	/**
	 * Lee el archivo indicado por path y llama los m�todos para construir estructuras
	 * @param	path	address of file to read.
	 * @throws	IOException	if an error occurred at reading
	 */
	public void readAndBuild(String path) throws IOException, Exception{
		File file = new File(path); 
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		fileName = file.getName().substring(0,file.getName().lastIndexOf('.'));
		ArrayList<String> pila = new ArrayList<String>();
		String readed = br.readLine();
		while(readed != null){
			pila.add(readed);
			readed = br.readLine();
		}
		
		agrupar(pila);
	}
	
	/**
	 * agrupa informacion de archivos por secciones
	 * @param strings	conjunto de cadenas
	 */
	private void agrupar(ArrayList<String> strings) throws Exception{
		int count = 0;
		String cadena = "";
		boolean[] centinela = new boolean[5];
		for (int n=0; n<centinela.length; n++) centinela[n] = true;
		
		//	SEGMENT COMPILER
		while (centinela[0]){
			cadena = strings.get(count);
			count++;
			if(cadena.contains("COMPILER")){
				String[] words = cadena.split(" ");
				for(int n=0;n<words.length-1;n++)
					if(words[n].equals("COMPILER"))
						projectName = words[n+1];
			}else if(cadena.contains("CHARACTERS"))
				centinela[0] = false;
			else if(cadena.contains("KEYWORDS"))
				for(int n=0;n<2;n++) centinela[n] = false;
			else if(cadena.contains("TOKENS"))
				for(int n=0;n<3;n++) centinela[n] = false;
			else if(cadena.contains("PRODUCTIONS"))
				for(int n=0;n<4;n++) centinela[n] = false;
			else if(cadena.contains("END") || count == strings.size())
				for(int n=0;n<5;n++) centinela[n] = false;
		}
		
		//	SEGMENT CHARACTERS
		Stack<String> characters = new Stack<String>();
		while (centinela[1]){
			cadena = strings.get(count);
			count++;
			if(cadena.contains("KEYWORDS"))
				for(int n=0;n<2;n++) centinela[n] = false;
			else if(cadena.contains("TOKENS"))
				for(int n=0;n<3;n++) centinela[n] = false;
			else if(cadena.contains("PRODUCTIONS"))
				for(int n=0;n<4;n++) centinela[n] = false;
			else if(cadena.contains("END") || count == strings.size())
				for(int n=0;n<5;n++) centinela[n] = false;
			else if(!cadena.equals(""))
				characters.add(cadena);
		}characterSet(characters);
		
		//	SEGMENT KEYWORDS
		Stack<String> keywords = new Stack<String>();
		while (centinela[2]){
			cadena = strings.get(count);
			count++;
			if(cadena.contains("TOKENS"))
				for(int n=0;n<3;n++) centinela[n] = false;
			else if(cadena.contains("PRODUCTIONS"))
				for(int n=0;n<4;n++) centinela[n] = false;
			else if(cadena.contains("END") || count == strings.size())
				for(int n=0;n<5;n++) centinela[n] = false;
			else if(!cadena.equals(""))
				keywords.add(cadena);
		}keywordsSet(keywords);
		
		//	SEGMENT TOKENS
		Stack<String> tokens = new Stack<String>();
		while (centinela[3]){
			cadena = strings.get(count);
			count++;
			if(cadena.contains("PRODUCTIONS"))
				for(int n=0;n<4;n++) centinela[n] = false;
			else if(cadena.contains("END") || count == strings.size())
				for(int n=0;n<5;n++) centinela[n] = false;
			else if(!cadena.equals(""))
				tokens.add(cadena);
		}tokensSet(tokens);
		
		//	SEGMENT PRODUCTIONS
		Stack<String> productions = new Stack<String>();
		while (centinela[4]){
			cadena = strings.get(count);
			count++;
			if(cadena.contains("END"))
				centinela[4] = false;
			else if(!cadena.equals(""))
				productions.add(cadena);
			
			if(count == strings.size())
				centinela[4] = false;
		}// no analizado
	}
	
	
	/**
	 * Crea conjuntos de caracteres.
	 * @param chars	lineas le�das relacionadas a conjuntos de caracteres
	 */
	private void characterSet(Stack<String> chars)throws Exception{
		charSets = new HashMap<String, ArrayList<Integer>>();
		for(int n=0; n<chars.size(); n++){
			if(chars.get(n).contains("=")){
				String nameSet = chars.get(n).substring(0, chars.get(n).indexOf('=')).trim();
				char[] stack = (chars.get(n).substring(chars.get(n).indexOf('=') +1)).trim().toCharArray();
				int lastChar = 0;
				boolean add = true, secuence = false;
				ArrayList<Integer> set = new ArrayList<Integer>();
				
				for(int m=0;m<stack.length;m++){
					String str = "";
					int chr = stack[m];
					switch (chr){
					
					//	Manejo de cadenas ( "String" )
					case '"':
						m++; chr = stack[m];
						while(chr!='"'){
							if(add)	{if(!set.contains(chr))set.add(chr);}
							else	remove(set, chr);
							m++;
							if(m==stack.length)	throw new Exception("Cerrar Comillas para declaracion de String");
							chr = stack[m];
						}break;
					
					//	Manejo de caracteres ( '[\] char ' )
					case '\'':
						if(!secuence){
							m++; chr = stack[m];
							lastChar=0;
							if(chr=='\\'){
								m++; chr = stack[m];
								if(stack[m+1]!='\'')
									throw new Exception("Caracter declarado no v�lido: '\\"+(char)chr+
											(char)stack[m+1]+" Se esperaba: '\\"+(char)chr+"'");
								switch (chr){
								case 'n':	lastChar='\n'; break;
								case 'r':	lastChar='\r'; break;
								case 't':	lastChar='\t'; break;
								case '\\':	lastChar='\\'; break;}
							}else{
								if(stack[m+1]!='\'')
									throw new Exception("Caracter declarado no v�lido: '"+(char)chr+
											(char)stack[m+1]+" Se esperaba: '"+(char)chr+"'");
								lastChar=chr;
							}m++;
							if(add)	{if(!set.contains(lastChar))set.add(lastChar);}
							else	remove(set, lastChar);
						}else{
							m++; chr = stack[m];
							int toChar = 0;
							if(chr=='\\'){
								m++; chr = stack[m];
								if(stack[m+1]!='\'')
									throw new Exception("Caracter declarado en CHARACTERS invalido: '\\"+(char)chr+
											(char)stack[m+1]+" Se esperaba: '\\"+(char)chr+"'");
								switch (chr){
								case 'n':	toChar='\n'; break;
								case 'r':	toChar='\r'; break;
								case 't':	toChar='\t'; break;
								case '\\':	toChar='\\'; break;}
							}else{
								if(stack[m+1]!='\'')
									throw new Exception("Caracter declarado en CHARACTERS invalido: '"+(char)chr+
											(char)stack[m+1]+" Se esperaba: '"+(char)chr+"'");
								toChar=chr;
							}m++;
							if(lastChar>toChar){
								int x = lastChar;
								lastChar = toChar;
								toChar = x;
							}
							for(int o=lastChar;o<=toChar;o++){
								if(add)	{if(!set.contains(o))set.add(o);}
								else	remove(set, o);
							}secuence = false;
						}
						break;
					
					//	Secuencia de caracteres o fin de linea
					case '.':
						if(m+1==stack.length || stack[m+1]=='.'){
							secuence = true;
							m++;
						}
						else m = stack.length;
						break;
						
					//	Agregar
					case '+':
						add = true;
						break;
					
					//	Eliminar
					case '-':
						add = false;
						break;
					
					//	Ignore
					case ' ':break;
					case '\t':break;
						
					//	Manejo de Ident, CHR y ANY
					default:
						while(chr!=' ' && chr!='+' && chr!='-' && chr!='.' && chr!='(' && chr!='\t'){
							str += (char)chr;
							m++; 
							if(m==stack.length)	break;
							chr = stack[m];
						}
						if(str.equals("ANY")){
							for(int o=0;o<MAX_CHAR;o++){
								if(add)	{if(!set.contains(o))set.add(o);}
								else	remove(set, o);
							}m--;
						}else if(str.equals("CHR")){
							if(!secuence){
								if(chr!='(')
									throw new Exception("CHR(n): despues de CHR se espera caracter '('.");
								m++; chr = stack[m];
								int o=m;
								while(stack[o]!=')')
									o++;
								String number = "";
								for(int p=m; p<o; p++)
									if( (stack[p]<48 || stack[p]>57))
										throw new Exception("CHR(n): n debe ser un numero.");
									else
										number += (char)stack[p];
								lastChar = Integer.parseInt(number.trim());
								if(lastChar<0 || n>MAX_CHAR)
									throw new Exception("CHR(n): n debe ser un entero entre 0 y "+MAX_CHAR+".");
								m=o;
								if(add)	{if(!set.contains(lastChar))set.add(lastChar);}
								else	remove(set, lastChar);
							}else{
								if(chr!='(')
									throw new Exception("CHR(n): despues de CHR se espera caracter '('.");
								m++; chr = stack[m];
								int o=m;
								while(stack[o]!=')')
									o++;
								String number = "";
								for(int p=m; p<o; p++)
									if( (stack[p]<48 || stack[p]>57))
										throw new Exception("CHR(n): n debe ser un numero.");
									else number += (char)stack[p];
								int toChar = Integer.parseInt(number.trim());
								if(toChar<0 || n>MAX_CHAR)
									throw new Exception("CHR(n): n debe ser un entero entre 0 y "+MAX_CHAR+".");
								m=o;								
								if(lastChar>toChar){
									int x = lastChar;
									lastChar = toChar;
									toChar = x;
								}
								for(int p=lastChar;p<=toChar;p++){
									if(add)	{if(!set.contains(p))set.add(p);}
									else	remove(set, p);
								}secuence = false;
							}
						}else{
							ArrayList<Integer> subSet = charSets.get(str);
							if(subSet == null)
								throw new Exception("El identificador '"+str+"' no hace referencia a un"
										+" conjunto declarado previamente.");
							for(int o=0;o<subSet.size();o++){
								if(add)	{if(!set.contains(subSet.get(o)))set.add(subSet.get(o));}
								else	remove(set, subSet.get(o));
							}
						}break;
					}
				}charSets.put(nameSet,set); //System.out.println(stack); System.out.println(set);
			}
		}
	}
	
	/**
	 * Remueve del conjunto 'set' el caracter 'chr'
	 * @param set	conjunto de caracteres
	 * @param chr	caracter a remover
	 * @return	true si se removi�, false si no se removi�
	 */
	private boolean remove(ArrayList<Integer> set, int chr){
		for(int n=0;n<set.size();n++)
			if(set.get(n)== chr){
				set.remove(n);
				return true;
			}
		return false;
	}
	
	private void keywordsSet (Stack<String> keywds) throws Exception{
		expReg = new HashMap<String, ArrayList<Integer>>();
		keys = new ArrayList<String>();
		for(int n=0;n<keywds.size();n++){
			if(keywds.get(n).contains("=")){
				String nameKW = keywds.get(n).substring(0, keywds.get(n).indexOf('=')).trim();
				String defKW = keywds.get(n).substring(keywds.get(n).indexOf('=') +1);
				
				if(defKW.indexOf('"') != defKW.lastIndexOf('"'))
					defKW = defKW.substring(defKW.indexOf('"')+1, defKW.lastIndexOf('"'));
				else
					throw new Exception("Declaracion incorrecta de KEYWORD: "+keywds.get(n)+"\n\t" +
							"Revise uso correcto de comillas.");
				if(defKW.equals("")){
					System.err.println("Warning: Declaracion incorrecta de KEYWORD: "+keywds.get(n)+
									   "\n\tSe asumira: "+nameKW+" = \""+nameKW+"\".");
					defKW = nameKW;
				}
				
				ArrayList<Integer> defER = new ArrayList<Integer>();
				addAll(defER, defKW.toCharArray());
				expReg.put(nameKW, defER);
				keys.add(nameKW);
			}
		}//System.out.println(defER);
	}
	
	private void tokensSet(Stack<String> tokens)throws Exception{
		ignorados = new ArrayList<Integer>(); 
		for(int n=0; n<tokens.size(); n++){
			if(tokens.get(n).contains("=")){
				String nameER = tokens.get(n).substring(0, tokens.get(n).indexOf('=')).trim();
				char[] stack = (tokens.get(n).substring(tokens.get(n).indexOf('=') +1)).trim().toCharArray();
				verificar(stack, '[', ']');
				verificar(stack, '{', '}');
				ArrayList<Integer> defER = buildDefER(stack, nameER);				
				expReg.put(nameER, defER);
				keys.add(nameER);
			}else if (tokens.get(n).contains("IGNORE")){
				String[] word = tokens.get(n).split(" ");
				if(word.length>2){
					for(int m=0;m<word.length-1;m++){
						if(word[m].equals("SET")){
							ignorados.addAll(charSets.get(word[m+1]));
						}
					}
				}
			}
		}
	}
	
	private void verificar(char[] pila, char char1, char char2) throws Exception{
		int chars = 0;
		for(int n=0; n<pila.length;n++){
			if (pila[n]==char1)
				chars++;
			else if (pila[n]==char2)
				chars--;
			if(chars<0)
				throw new Exception("Declaracion incorrecta de token.\n\tVerificar caracteres: "+char2+".");
		}
		if(chars!=0)
			throw new Exception("Declaracion incorrecta de token.\n\tVerificar caracteres: "+char1+".");
	}
	
	private ArrayList<Integer> buildDefER(char[] stack, String name) throws Exception{
		ArrayList<Integer> defER = new ArrayList<Integer>();
		for(int n=0;n<stack.length;n++){
			char chr = stack[n];
			switch (chr){
			
			//	Manejo de cadenas ( \"String\" )
			case '"':
				n++; chr = stack[n];
				while(chr!='"'){
					defER.add((int)chr);
					n++;
					if(n==stack.length)	throw new Exception("Cerrar Comillas para declaracion de String en TOKEN: "+ name);
					chr = stack[n];
				}
				break;
			
			//	Manejo de caracteres ( '[\] char ' )
			case '\'':

				n++; chr = stack[n];
				
				if(chr=='\\' && stack[n+2]!='\''){
					throw new Exception("Caracter declarado en token invalido: '\\"+(char)chr+
							(char)stack[n+1]+" Se esperaba: '\\"+(char)chr+"'");
				}else{
					switch(stack[n+1]){
					case '\\':	defER.add((int)'\\');	break;
					case '\'':	defER.add((int)'\'');	break;
					case '\t':	defER.add((int)'\t');	break;
					case '\n':	defER.add((int)'\n');	break;
					case '\r':	defER.add((int)'\r');	break;
					default:	break;
					}n+=2;
				}
				
				if (stack[n+1]!='\''){
					throw new Exception("Caracter declarado en token invalido: '\\"+(char)chr+
							(char)stack[n+1]+" Se esperaba: '"+(char)chr+"'");
				}else{
					defER.add((int)chr);
					n++;
				}break;
			
			//	Manejo de pipe
			
			case '|':
				defER.add(ExpresionRegular.pip); break;
				
			//	Fin de declaracion de token
			case '.':
				n = stack.length;
				break;
				
			//	Manejo de llaves { }
			case '{':
				int count = 1, m = n;
				while(count!=0 && m<stack.length-1){
					m++;
					if(stack[m]=='{')
						count++;
					else if(stack[m]=='}')
						count--;
				}
				defER.add(ExpresionRegular.par1);
				defER.addAll(buildDefER(subpila(stack,n,m), name));
				defER.add(ExpresionRegular.par2);
				defER.add(ExpresionRegular.ast);
				n=m;	break;
				
			//	Manejo de corchetes [ ]
			case '[':
				int llaves = 1, o = n;
				while(llaves!=0 && o<stack.length-1){
					o++;
					if(stack[o]=='[')
						llaves++;
					else if(stack[o]==']')
						llaves--;
				}
				defER.add(ExpresionRegular.par1);
				defER.add(ExpresionRegular.par1);
				defER.addAll(buildDefER(subpila(stack,n,o), name));
				defER.add(ExpresionRegular.par2);
				defER.add(ExpresionRegular.pip);
				defER.add(ExpresionRegular.eps);
				defER.add(ExpresionRegular.par2);
				n=o;	break;
				
			//	Ignore
			case ' ':	break;
			case '\t':	break;
				
			//	Manejo de Identificadores
			default:
				String str = "";
				while(chr!=' '&&chr!='\t'&&chr!='['&&chr!=']'&&chr!='\"'&&chr!='\''&&chr!='{'&&chr!='}'&&chr!='.'&&chr!='|'){
					str += chr;
					n++;
					if(n==stack.length)
						break;
					chr = stack[n];
				}n--;
				if(str.equals("EXCEPT") || str.equals("KEYWORDS"))
					break;
				ArrayList<Integer> set = charSets.get(str);
				if(set == null)
					throw new Exception("El identificador '"+str+"' no hace referencia a un conjunto declarado previamente.");
				else{
					if(set.size()>1){
						defER.add(ExpresionRegular.par1);
						for(int p=0;p<set.size()-1;p++){
							defER.add(set.get(p));
							defER.add(ExpresionRegular.pip);
						}defER.add(set.get(set.size()-1));
						defER.add(ExpresionRegular.par2);
					}else if(set.size()==1)
						defER.add(set.get(0));
				}break;
			}
		}return defER;
	}
	
	/**
	 * Crea una subpila de la pila recibida
	 * @param pila	pila recibida
	 * @param index1	indice donde empezar la subpila
	 * @param index2	indice donde terminar la subpila
	 * @return	subpila
	 */
	private char[] subpila(char[] pila, int index1, int index2){
		int index = index1+1;
		char[] subpila = new char[index2-index];
		for (int n=0; n<(index2-index1-1);n++){
			subpila[n] = pila[index];
			index++;
		}
		return subpila;
	}
	
	/**
	 * Agrega los elementos del Array de Caracteres al ArrayList de enteros
	 * @param pila	Array de caracteres de donde extraer datos 
	 * @param pilita	ArrayList de entero donde almacenar datos
	 */
	private void addAll(ArrayList<Integer> pila, char[] pilita){
		for(int n=0;n<pilita.length;n++){
			pila.add((int)pilita[n]);
		}
	}
	
	/**
	 * Retorna las llaves, nombres de los tokens, que asocia al elemento del hashmap, Expresiones regulares.
	 * @return keys 	Array de Strings
	 */
	
	public ArrayList<String> getKeys(){
		return keys;
	}
}