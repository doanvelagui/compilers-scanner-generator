package scannerGenerator;

import java.io.*;
import java.util.*;
import dataStructures.*;
import automataGenerator.ExpresionRegular;

public class Lexer implements Serializable {

	//	CONSTANTES
	
	/** Serial Version */
	private static final long serialVersionUID = 80;
	/** Epsilon  */
	private static final Integer eps = ExpresionRegular.eps;
	
	//	VARIABLES
	
	/** Grafo */
	private Grafo grafo;
	/** Tokens */
	private HashMap<Integer, String> tokens;
	/** Estado inicial */
	private Integer estadoInicial;
	/** EStados de aceptacion */
	private Integer[] estadoFinal;
	
	/** Definicion de nontoken encontrado */
	private String nonToken;
	/** Bandera para nonToken */
	private boolean nonTokenFind;
	
	/** Definicion de token encontrado */
	private String token;
	/** Tipo de token encontrado */
	private Integer tokenType;
	/** Bandera para Token Encontrado */
	private boolean tokenFind;
	
	/** Bandera para error encontrado */
	private boolean error;
	/** Conjunto de estados */
	private ArrayList<Integer> conjunto;
	/** Caracteres ignorados */
	private ArrayList<Integer> ignore;
	
	//	METODOS
	
	/**
	 * Constructor de la clase
	 */
	public Lexer(Grafo grafo, HashMap<Integer, String> tokens, ArrayList<Integer> ignore){
		this.grafo = grafo;
		this.tokens = tokens;
		this.ignore = ignore;
		estadoInicial = grafo.estadoInicial;
		estadoFinal = new Integer[grafo.estadosFinales.size()];
		grafo.estadosFinales.toArray(estadoFinal);
		
		nonToken = "";
		nonTokenFind = false;
		error = true;
	}
	
	public void tokenizer(String path){
		ArrayList<Integer> buffer = new ArrayList<Integer>();
		try{
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			int readed = br.read();
			while(readed!=-1){
				buffer.add(readed);
				readed = br.read();
			}
			br.close();
			buffer.add(-1);
		}catch(Exception e){System.err.println("ERROR: "+e.getMessage());}
		
		int n = 0;
		while(n<buffer.size()){
			Integer caracter = buffer.get(n);
			n++;
			if(!ignore.contains(caracter)){
				if(token(caracter)){
					if(tokenFind){
						System.out.println("Token Encontrado:\t"+tokens.get(tokenType)+((tokens.get(tokenType).length()<8)? "\t":"")+"\t"+token);
						n--;
					}else{
						if(nonTokenFind)
							nonToken += (char)(int)caracter;
						else{
							nonTokenFind = true;
							nonToken = ""+(char)(int)caracter;
						}
					}
				}else{
					if(nonTokenFind){
						nonTokenFind = false;
						System.out.println("Token no Identificado:\t"+nonToken);
					}
				}
			}else{
				if(tokenFind)
					System.out.println("Token Encontrado:\t"+tokens.get(tokenType)+((tokens.get(tokenType).length()<8)? "\t":"")+"\t"+token);
				error = true;
				tokenFind = false;
			}
		}
	}
	private boolean token(Integer caracter){
		if(error){
			error = false;
			conjunto = new ArrayList<Integer>();
			conjunto.add(estadoInicial);
			conjunto = cerraduraEps(conjunto);
			token = "";
			tokenType = -1;
			tokenFind = false;
		}
		conjunto = mueve(conjunto, caracter);
		if(conjunto.isEmpty()){
			error = true;
			return error;
		}
		token += (char)(int)(caracter);
		boolean finded = true;
		for(int n=0;n<estadoFinal.length && finded;n++){
			if(conjunto.contains(estadoFinal[n])){
				tokenFind = true;
				finded = false;
				tokenType = estadoFinal[n];
			}
		}return error;
	}
	
	/**
	 * Cerradura Epsilon
	 * @param automata, automata finito no determinista
	 * @param estados, conjunto
	 * @return cerradura, conjunto
	 */
	private ArrayList<Integer> cerraduraEps(ArrayList<Integer> estados){
		if(estados!=null){
			ArrayList<Integer> pila = new ArrayList<Integer>();
			pila.addAll(estados);
			ArrayList<Integer> subpila = new ArrayList<Integer>();
			subpila.addAll(estados);
			
			while(!subpila.isEmpty()){
				Integer estado = subpila.remove(subpila.size()-1);
				LinkedList<Enlace<Integer>> edges = grafo.getEdges(estado);
				if(edges!=null){
					for(int n=0;n<edges.size();n++){
						Enlace<Integer> e = edges.get(n);
						if(e.getPeso().equals(eps)){
							if(!pila.contains(e.getDestino())){
								pila.add(e.getDestino());
								subpila.add(e.getDestino());
							}
						}
					} 
				}
			}
			return pila;
		}else return null;
	}
	
	/**
	 * Mueve
	 * @param automata, automata finito determinista
	 * @param estados, conjunto 
	 * @param transicion, (int)caracter
	 * @return mueve, conjunto
	 */
	private ArrayList<Integer> mueve(ArrayList<Integer> estados, Integer transicion){
		if(estados!=null){
			ArrayList<Integer> pila = new ArrayList<Integer>();
			ArrayList<Integer> subpila = new ArrayList<Integer>();
			subpila.addAll(estados);
			while(!subpila.isEmpty()){
				Integer estado = subpila.remove(subpila.size()-1);
				LinkedList<Enlace<Integer>> edges = grafo.getEdges(estado);
				if(edges!=null){
					for(int n=0;n<edges.size();n++){
						Enlace<Integer> e = edges.get(n);
						if(e.getPeso().equals(transicion)){
							if(!pila.contains(e.getDestino())){
								pila.add(e.getDestino());
							}
						}
					} 
				}
			}
			return pila;
		}else return null;
	}
}
